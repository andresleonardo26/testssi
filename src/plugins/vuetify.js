import Vue from "vue";
import Vuetify from "vuetify/lib";
import { Resize } from "vuetify/lib/directives";

Vue.use(Vuetify);

export default new Vuetify({
  directives: {
    Resize
  },
  icons: {
    iconfont: "mdi"
  },
  theme: {
    themes: {
      light: {
        primary: "#90A4AE",
        secondary: "#AB47BC",
        accent: "#9E9D24",
        error: "#D50000",
        info: "#81D4FA",
        success: "#1B5E20",
        warning: "#EF6C00",
        light: "#F3E5F5",
        dark: "#424242"
      },
      dark: {
        primary: "#90A4AE",
        secondary: "#AB47BC",
        accent: "#9E9D24",
        error: "#D50000",
        info: "#81D4FA",
        success: "#1B5E20",
        warning: "#EF6C00",
        light: "#F3E5F5",
        dark: "#424242"
      }
    }
  }
});
